package com.epam.shapes;

public class Rectangle extends Shape{

    public Rectangle(String shape, String color) {
        super(shape, color);
    }

    public double square(double a, double b) {
        System.out.println("Square of rectangle: ");
        return  a * b;
    }
}
