package com.epam.shapes;

public  class Triangle extends Shape {
    public Triangle(String shape, String color) {
        super(shape, color);
    }

    public double square(double a, double ha){
        System.out.println("Square of triangle: ");
        return (a * ha)/2;
    }
}
