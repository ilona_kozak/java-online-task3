package com.epam.animal;

public abstract class Fish implements Animal{
    String fishSpecies;
    boolean branchia;
    boolean squama;
    String color;
    boolean predator;

    abstract void swim();
}
