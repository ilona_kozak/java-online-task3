package com.epam.animal;

public abstract class Bird implements Animal{

    Food food;
    String birdSpecies;
    String color;
    boolean predator;

    public Bird(String birdSpecies, String color, boolean predator, Food food) {
        this.birdSpecies = birdSpecies;
        this.color = color;
        this.predator = predator;
        this.food = food;
    }

    public String getBirdSpecies(){
        return birdSpecies;
    }

    void doHave(boolean feathers, boolean wings) {

    }

    abstract void fly();
}
