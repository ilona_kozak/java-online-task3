package com.epam.animal;

public class Eagle extends Bird {

    public Eagle(String birdSpecies, String color, boolean predator, Food food) {
        super(birdSpecies, color, predator, food);
    }

    public void fly() {
        System.out.println("I'm " + birdSpecies+ ", I'm flying now for eating you!");
    }

    public void sleep() {
        System.out.println("It will be a long night, bye... zzz... zzz");
    }

    public void eatAnything() {
        System.out.println("You'll never get out of my beak! Come here, " + food);
    }

    @Override
    public void doHave(boolean feathers, boolean wings) {
        super.doHave(feathers, wings);
        System.out.println("Do I have feathers? -- " + feathers);
        System.out.println("Do I have wings? -- " + wings);
    }

    @Override
    public String toString() {
        return "Eagle{" +
                "food=" + food +
                ", birdSpecies='" + birdSpecies + '\'' +
                ", color='" + color + '\'' +
                ", predator=" + predator +
                '}';
    }
}
