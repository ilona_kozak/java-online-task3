package com.epam.animal;

public class Parrot extends Bird {

    public Parrot(String birdSpecies, String color, boolean predator, Food food) {
        super(birdSpecies, color, predator, food);
    }

    public void fly() {
        System.out.println("I'm " + birdSpecies+ " and I can flyyyy!");
    }

    public void sleep() {
        System.out.println("Ok, bye... zzz... zzz");
    }

    public void eatAnything() {
        System.out.println("Give me your " + food + ", bastard!");
    }

    @Override
    public void doHave(boolean feathers, boolean wings) {
        super.doHave(feathers, wings);
        System.out.println("Do I have feathers? -- " + feathers);
        System.out.println("Do I have wings? -- " + wings);
    }

    @Override
    public String toString() {
        return "Parrot{" +
                "food=" + food +
                ", birdSpecies='" + birdSpecies + '\'' +
                ", color='" + color + '\'' +
                ", predator=" + predator +
                '}';
    }
}
