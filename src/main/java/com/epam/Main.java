package com.epam;

import com.epam.animal.Eagle;
import com.epam.animal.Food;
import com.epam.animal.Parrot;
import com.epam.shapes.Circle;
import com.epam.shapes.Rectangle;
import com.epam.shapes.Triangle;

import java.util.*;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();
    public static void main(String[] args) {
        List<Parrot> parrots = new ArrayList<Parrot>();
        parrots.add(new Parrot("Macaw", "blue", false, Food.FRUITS));
        parrots.add(new Parrot("Kea", "green", true, Food.INSECTS));
        parrots.add(new Parrot("Cockatoo", "white", false, Food.NUTS));
        parrots.add(new Parrot("Rosella", "red", false, Food.SEEDS));
        List<Eagle> eagles = new ArrayList<Eagle>();
        eagles.add(new Eagle("Sea Eagle", "white", true, Food.FISH));
        eagles.add(new Eagle("Banded Snake-Eagle", "grey", true, Food.SNAKE));
        eagles.add(new Eagle("Black eagle", "black", true, Food.BIRD));
        eagles.add(new Eagle("Macaw", "brown", true, Food.SNAKE));

        Iterator<Parrot> iterator = parrots.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println();
        Iterator<Eagle> iterator1 = eagles.iterator();
        while(iterator1.hasNext()){
            System.out.println(iterator1.next());
        }

        int a = 0;
        int b = parrots.size() - 1;
        int c = eagles.size() - 1;

        for (int i = 0; i < parrots.size() || i < eagles.size(); i++){
            System.out.println("\n" + parrots.get(i).getBirdSpecies());
            parrots.get(i).doHave(Main.getRandomBoolean(), Main.getRandomBoolean());
            System.out.println("\n" + eagles.get(i).getBirdSpecies());
            eagles.get(i).doHave(Main.getRandomBoolean(), Main.getRandomBoolean());
        }

        for (int i = 0; i < parrots.size() || i < eagles.size(); i++){
            int random_number1 = a + (int) (Math.random() * b);
            int random_number2 = a + (int) (Math.random() * c);
            System.out.println();
            parrots.get(random_number1).fly();
            parrots.get(random_number1).sleep();
            parrots.get(random_number1).eatAnything();
            System.out.println();
            eagles.get(random_number2).fly();
            eagles.get(random_number2).sleep();
            eagles.get(random_number2).eatAnything();
        }

        List<Rectangle> rectangles = new ArrayList<Rectangle>();
        rectangles.add(new Rectangle("Rectangle", "blue"));
        rectangles.add(new Rectangle("Rectangle", "red"));
        rectangles.add(new Rectangle("Rectangle", "yellow"));
        List<Circle> circles = new ArrayList<Circle>();
        circles.add(new Circle("Circle", "blue"));
        circles.add(new Circle("Circle", "red"));
        circles.add(new Circle("Circle", "green"));
        List<Triangle> triangles = new ArrayList<Triangle>();
        triangles.add(new Triangle("Triangle", "orange"));
        triangles.add(new Triangle("Triangle", "grey"));
        triangles.add(new Triangle("Triangle", "purple"));
        Iterator<Rectangle> rectangleIterator = rectangles.iterator();
        Iterator<Circle> circleIterator = circles.iterator();
        Iterator<Triangle> triangleIterator = triangles.iterator();
        System.out.println();
        while(rectangleIterator.hasNext()){
            System.out.println(rectangleIterator.next());
        }
        System.out.println();
        while(circleIterator.hasNext()){
            System.out.println(circleIterator.next());
        }
        System.out.println();
        while(triangleIterator.hasNext()){
            System.out.println(triangleIterator.next());
        }

        double x = 100, y = 100, z = 100;
        System.out.println();
        for (int i = 0; i < rectangles.size() || i < circles.size() ||i < triangles.size(); i++){
            int random_number1 = a + (int) (Math.random() * x);
            int random_number11 = a + (int) (Math.random() * x);
            int random_number2 = a + (int) (Math.random() * y);
            int random_number3 = a + (int) (Math.random() * z);
            int random_number33 = a + (int) (Math.random() * z);
            System.out.println(rectangles.get(i).square(random_number1, random_number11));
            System.out.println(circles.get(i).square(random_number2));
            System.out.println(triangles.get(i).square(random_number3, random_number33));
        }
    }

    public static boolean getRandomBoolean() {
        return random.nextBoolean();
    }
}
